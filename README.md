# Repository "hearts" 
contains programs for the game Hearts.

### Owner: 
Anna

## srt_to_dict

### Function turn_str_to_dict(in_string)

Turns a string into dictionary where keys are all symbols in the string
and values are frequencies of symbols in the string

Example:

Enter a string: Frequency   
Frequency:   
F : 0.1111111111111111   
r : 0.1111111111111111   
e : 0.2222222222222222   
q : 0.1111111111111111   
u : 0.1111111111111111   
n : 0.1111111111111111   
c : 0.1111111111111111   
y : 0.1111111111111111   
Dictionary:   
{'F': 0.1111111111111111, 'r': 0.1111111111111111, 'e': 0.2222222222222222, 'q': 0.1111111111111111, 'u': 0.1111111111111111, 'n': 0.1111111111111111, 'c': 0.1111111111111111, 'y': 0.1111111111111111}

## ternar.py

### Function do_ternary(x, y)

Returns a string result according to conditions:   
if x < y, the result is x + y,   
if x == y, the result is 0,   
if x > y, the result is x - y,   
if x == 0 and y == 0, the result is "game over"

Example:

x: 1   
y: 2   
3

x: 1   
y: 1   
0

x: 2  
y: 1  
1

x: 0  
y: 0  
game over
