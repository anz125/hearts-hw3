# за допомогою тернарного оператора релізувати логіку:
# є параметри x та у,
# якщо x < y - друкуємо x + y,
# якщо x == y - друкуємо 0,
# якщо x > y - друкуємо x - y,
# якщо x == 0 та y == 0 друкуємо "game over"


"""Returns a string result according to conditions"""


def do_ternary(x: int, y: int) -> str:
    return str("game over" if x == 0 & y == 0 else x + y if x < y else x - y)
    # x + y if x < y else x - y if x > y else "game over" if x == 0 & y == 0 else 0


if __name__ == "__main__":
    cases = (
        (1, 2, "3"),
        (1, 1, "0"),
        (2, 1, "1"),
        (0, 0, "game over"),
    )

    for x, y, result in cases:
        p = do_ternary(x, y)
        assert (
            p == result
        ), f"ERROR: do_ternary({x}, {y} returned {p} but expected {result})"

    x = int(input("x: "))
    y = int(input("y: "))

    p = do_ternary(x, y)

    print(p)
