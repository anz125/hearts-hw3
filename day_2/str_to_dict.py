# написати функцію якам приймає рядок і повертає словник у якому
# ключами є всі символи, які зустрічаються в цьому рядку, а значення - відповідні
# вірогідності зустріти цей символ в цьому рядку.
# № код повинен бути структурований за допомогою конструкції if name == "__main__":,
# всі аргументи і значення що функція повертає повинні бути типізовані, функція має рядок документації

"""Turns a string into dictionary where keys are all symbols in the string
and values are frequencies of symbols in the string"""


def turn_str_to_dict(in_string: str) -> dict:
    len_in_string = len(in_string)
    symbols = tuple(in_string)
    res_dict = {}
    for symbol in symbols:
        num = in_string.count(symbol)
        res_dict.update({symbol: num / len_in_string})
    return res_dict


if __name__ == "__main__":
    cases = (
        ("1234", {"1": 0.25, "2": 0.25, "3": 0.25, "4": 0.25}),
        ("aa11", {"a": 0.5, "1": 0.5}),
        (
            "aabc1125",
            {"a": 0.25, "b": 0.125, "c": 0.125, "1": 0.25, "2": 0.125, "5": 0.125},
        ),
        (
            "Frequency",
            {
                "F": 0.1111111111111111,
                "r": 0.1111111111111111,
                "e": 0.2222222222222222,
                "q": 0.1111111111111111,
                "u": 0.1111111111111111,
                "n": 0.1111111111111111,
                "c": 0.1111111111111111,
                "y": 0.1111111111111111,
            },
        ),
    )

    input_string = input("Enter a string: ")
    result_dict = turn_str_to_dict(input_string)
    print("Frequency: ")

    for key, value in result_dict.items():
        print(key, ": ", value)

    print("Dictionary:")
    print(result_dict)
